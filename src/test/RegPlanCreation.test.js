import React from "react";
import { Component } from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import axios from 'axios';
import RegPlanCreation from "../Pages/RegPlanCreation";
jest.mock('axios');


configure({adapter: new Adapter()});

describe('form', () => {
    it('rendering app', () => {
      const wrapper = shallow(<RegPlanCreation/>);
      wrapper.find('input').at(0).simulate('change', { target: {value : "title"}});
      wrapper.find('input').at(1).simulate('change', { target: {value : "description"}});
      wrapper.find('input').at(2).simulate('change', { target: {value : "relevance"}});
      wrapper.find('input').at(3).simulate('change', { target: {value : "days"}});
      expect(wrapper.instance().state.title).toEqual("title");
      expect(wrapper.instance().state.description).toEqual("description");
      expect(wrapper.instance().state.relevance).toEqual("relevance");
      expect(wrapper.instance().state.days).toEqual([{"number": 1}]);
    });
  });

