import React from "react";
import { configure, shallow } from 'enzyme';
import Plans from './../Pages/Plans';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering plans', () => {
    const wrapper = shallow(<Plans />);
    expect(wrapper.find('div')).toHaveLength(5);
    expect(wrapper.find('h1')).toHaveLength(1);
  });

});
