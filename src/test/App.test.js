import React from "react";
import { configure, shallow } from 'enzyme';
import App from './../App';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {
  it('rendering app', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('div')).toHaveLength(0);
  });
});
