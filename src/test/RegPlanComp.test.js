import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import RegPlanComp from './../Components/RegPlanComp';

configure({ adapter: new Adapter() });

describe('test', () => {
    it('test1', () => {
        const regPlan = {
            "title": "Пойти к бабушке",
            "description": "Взять пирожки",
            "relevance": 1,
            "days": ['ВТ', 'ПТ']
        }

        const wrapper = shallow(<RegPlanComp title={regPlan.title} description={regPlan.description} relevance={regPlan.relevance} days ={regPlan.days} />);
        expect(wrapper.find('li').find('div').at(0).text()).toEqual('----Пойти к бабушке----');
        expect(wrapper.find('li').find('div').at(1).text()).toEqual('Description: Взять пирожки');
        expect(wrapper.find('li').find('div').at(2).text()).toEqual('Relevance: 1');
        expect(wrapper.find('li').find('div').at(3).text()).toEqual('Days: ВТ ПТ ');
    })
})