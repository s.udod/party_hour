import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import UnitPlanComp from './../Components/UnitPlanComp';

configure({ adapter: new Adapter() });

describe('test', () => {
    it('test1', () => {
        const up = {
            "title": "Сходить к бабушке Люде",
            "date" : "23.06.2020",
            "relevance": 2,
            "description": "Взять пирожки"            
        }

        const wrapper = shallow(<UnitPlanComp title={up.title} date={up.date} description={up.description} relevance={up.relevance} />);
        expect(wrapper.find('li').find('div').at(0).text()).toEqual('----Сходить к бабушке Люде----');
        expect(wrapper.find('li').find('div').at(1).text()).toEqual('Date: 23.06.2020');
        expect(wrapper.find('li').find('div').at(2).text()).toEqual('Relevance: 2');
        expect(wrapper.find('li').find('div').at(3).text()).toEqual('Description: Взять пирожки');
    })
})