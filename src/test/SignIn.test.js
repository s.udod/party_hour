import React from "react";
import SignIn from '../Pages/SignIn';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import axios from 'axios';
jest.mock('axios');


configure({adapter: new Adapter()});

describe('form', () => {
  it('rendering app', () => {
    const wrapper = shallow(<SignIn />);
    wrapper.find('input').at(0).simulate('change', { target: {value : "test@test.test"}});
    expect(wrapper.instance().state.userName).toEqual("test@test.test")
  });
});

describe('api test', () => {
    it('successful login with an API', async () => {
        const data = {
            data: "mocked_token"
        };
        const wrapper = shallow(<SignIn/>);
        const inputs = wrapper.find('input')
        inputs.at(0).simulate('change', {target: {name: "username", value: "test@test.test"}});
        axios.post.mockImplementationOnce(() => Promise.resolve(data));
        wrapper.find('Button').props().onClick() ;
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(wrapper.instance().state.requestResult).toEqual("success");
        expect(wrapper.instance().state.loggedIn).toBeTruthy();
    });
    it('wrong login with an API', async () => {
        const errorMessage = 'Network Error';
        const wrapper = shallow(<SignIn/>);
        const inputs = wrapper.find('input')
        inputs.at(0).simulate('change', { target: {name : "username", value : "test@test.test"}});
        axios.post.mockImplementationOnce(() =>
            Promise.reject(new Error(errorMessage)),
        );
        wrapper.find('Button').props().onClick();
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(wrapper.instance().state.requestResult).toEqual("bad data");
        expect(wrapper.instance().state.loggedIn).toBeFalsy();
    });
});
