import React from "react";
import { Component } from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import axios from 'axios';
import SignUp from "../Pages/SignUp";
jest.mock('axios');


configure({adapter: new Adapter()});

describe('form', () => {
  it('rendering app', () => {
    const wrapper = shallow(<SignUp/>);
    wrapper.find('input').at(0).simulate('change', { target: {value : "userName"}});
    wrapper.find('input').at(1).simulate('change', { target: {value : "firstName"}});
    wrapper.find('input').at(2).simulate('change', { target: {value : "secondName"}});
    wrapper.find('input').at(3).simulate('change', { target: {value : "pictureURL"}});
    expect(wrapper.instance().state.userName).toEqual("userName");
    expect(wrapper.instance().state.firstName).toEqual("firstName");
    expect(wrapper.instance().state.secondName).toEqual("secondName");
    expect(wrapper.instance().state.pictureURL).toEqual("pictureURL");
  });
});

describe('api test', () => {
    it('successful signUp with an API', async () => {
        const data = {
            data: "mocked_token"
        };
        const wrapper = shallow(<SignUp/>);
        wrapper.find('input').at(0).simulate('change', { target: {value : "userName"}});
        wrapper.find('input').at(1).simulate('change', { target: {value : "firstName"}});
        wrapper.find('input').at(2).simulate('change', { target: {value : "secondName"}});
        wrapper.find('input').at(3).simulate('change', { target: {value : "pictureURL"}});
        axios.post.mockImplementationOnce(() => Promise.resolve(data));
        wrapper.find('Button').simulate('click');
        //wrapper.find('Button').props().onClick() ;
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(wrapper.instance().state.requestResult).toEqual("success");
    });
    it('wrong signUp with an API', async () => {
        const errorMessage = 'Network Error';
        const wrapper = shallow(<SignUp/>);
        wrapper.find('input').at(0).simulate('change', { target: {value : "userName"}});
        wrapper.find('input').at(1).simulate('change', { target: {value : "firstName"}});
        wrapper.find('input').at(2).simulate('change', { target: {value : "secondName"}});
        wrapper.find('input').at(3).simulate('change', { target: {value : "pictureURL"}});
        axios.post.mockImplementationOnce(() =>
            Promise.reject(new Error(errorMessage)),
        );
        wrapper.find('Button').props().onClick() ;
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(wrapper.instance().state.requestResult).toEqual("bad data");
    
    });
});


