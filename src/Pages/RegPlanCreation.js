import { Form, Button } from "react-bootstrap";
import React, { Component } from 'react';
import axios from 'axios';


export default class RegPlanCreation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            relevance: "",
            days: [],
            Sunday: false,
            Monday: false,
            Tuesday: false,
            Wednesday: false,
            Thursday: false,
            Friday: false,
            Saturday: false,
            buttonEnabled: true,
            requestResult: ""
        };
        this.handlePlanNameChange = this.handlePlanNameChange.bind(this);
        this.handlePlanDescriptionChange = this.handlePlanDescriptionChange.bind(this);
        this.handlePlanRelevanceChange = this.handlePlanRelevanceChange.bind(this);
        this.handleForm = this.handleForm.bind(this);
        this.addMonday=this.addMonday.bind(this);
        this.addTuesday=this.addTuesday.bind(this);
        this.addWednesday=this.addWednesday.bind(this);
        this.addThursday=this.addThursday.bind(this);
        this.addFriday=this.addFriday.bind(this);
        this.addSaturday=this.addSaturday.bind(this);
        this.addSunday=this.addSunday.bind(this);
    }

    handlePlanNameChange(event) {
        this.setState({
            title: event.target.value
        });
    }
    handlePlanDescriptionChange(event) {
        this.setState({
            description: event.target.value
        });
    }
    handlePlanRelevanceChange(event) {
        this.setState({
            relevance: event.target.value
        });
    }
    handleDaysChange(event) {
        this.setState({
           days: event.target.value
        });
    }
    
    addMonday(event){
        this.setState({
            days: this.state.days.concat({number:1})
          })
         }
    addTuesday(event){
        this.setState({
            days: this.state.days.concat({number:2})
          })
    }
    addWednesday(event){
        this.setState({
            days: this.state.days.concat({number:3})
        });
    }
    addThursday(event){
        this.setState({
            days: this.state.days.concat({number:4})
        });
    }
    addFriday(event){
        this.setState({
            days: this.state.days.concat({number:5})
        });
    }
    addSaturday(event){
        this.setState({
            days: this.state.days.concat({number:6})
        });
    }
    addSunday(event){
        this.setState({
            days: this.state.days.concat({number:7})
        });
    }

    getData() {
        return localStorage.getItem('userId');
    }


    handleForm(event) {
        this.setState({
            buttonEnabled: false,
            requestResult: "creation.."
        });

        
    
        const headers = {
            'userID': this.getData() 
          }
        axios.post('http://34.89.147.190:8080/api/v001/partyhour/regplans',
            {
               
                title: this.state.title,
                description: this.state.description,
                relevance: this.state.relevance,
                days: this.state.days
            },
           { headers: headers}
            )
            
            .then(r => {
                console.log(r)
                this.setState({ requestResult: "success" })
                console.log("ddd") 
                
            })
           .catch(e => {
                console.log(e)
               console.log(this.state.days)
              // console.log(this.state.relevance)
             //   this.setState({ requestResult: "fill in all input fields" })
            });
        this.setState({ buttonEnabled: true })
        event.preventDefault();
    }
    render() {
        return (
            <div className="reg">
                <h1> New regular plan </h1>
                <Form vertical="true">
                    <input
                        className="form-control"
                        type="text"
                        placeholder="Plan Name"
                        value={this.state.title}
                        onChange={this.handlePlanNameChange}
                        required={true}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="Plan Description"
                        value={this.state.description}
                        onChange={this.handlePlanDescriptionChange}
                        required={true}
                    />
                    <label>
                        Please, choose your plan relevanse.
                        1 -   high importance
                        2 -   medium importance
                        3 -   low importance
            
                    <input
                            className="form-control"
                            type="number"
                            placeholder="Plan Relevance"
                            value={this.state.relevance}
                            onChange={this.handlePlanRelevanceChange}
                            required={true}
                        /> 
                    
                   
                    </label>
                    <label>
                    Please, choose days.
                    <input
                        className="form-control"
                        type="checkbox" 
                        value={this.state.Monday}  
                        //onChange={this.addMonday.bind(this)}
                        onClick={this.addMonday.bind(this)}
                        onChange={this.addMonday.bind(this)}
                    />Monday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Tuesday}
                        //onChange={this.addTuesday.bind(this)}
                        onClick={this.addTuesday.bind(this)} 
                        onChange={this.addTuesday.bind(this)}

                    />Tuesday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Wednesday}
                        onChange={this.addWednesday.bind(this)}
                        onClick={this.addWednesday.bind(this)}
                    />Wednesday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Thursday}
                        onChange={this.addThursday.bind(this)}
                       onClick={this.addThursday.bind(this)}
                    />Thursday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Friday}
                       onChange={this.addFriday.bind(this)}
                      onClick={this.addFriday.bind(this)}
                    />Friday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Saturday}
                       onChange={this.addSaturday.bind(this)}
                      onClick={this.addSaturday.bind(this)}
                    />Saturday
                    <input
                        className="form-control"
                        type="checkbox"
                        value={this.state.Sunday}
                        onChange={this.addSunday.bind(this)}
                        onClick={this.addSunday.bind(this)}
                    />Sunday
                </label>
                <div className="row justify-content-center">
                    <Button
                        className="btn btn-primary"
                        type="submit"
                        disabled={!this.state.buttonEnabled}
                        onClick={this.handleForm}
                    >
                        Create
                        </Button>
                </div>
                </Form>
                <div>
                    {this.state.requestResult}
                </div>





            </div>
        )
    }
}
