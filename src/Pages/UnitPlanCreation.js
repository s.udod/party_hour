import { Form, Button } from "react-bootstrap";
import React, { Component } from 'react';
import axios from 'axios';

export default class UnitPlanCreation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            date: "",
            description: "",
            relevance: "",
            buttonEnabled: true,
            requestResult: ""
        };
        this.handlePlanNameChange = this.handlePlanNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handlePlanDescriptionChange = this.handlePlanDescriptionChange.bind(this);
        this.handlePlanRelevanceChange = this.handlePlanRelevanceChange.bind(this);
        this.handleForm = this.handleForm.bind(this);
    }

    handlePlanNameChange(event) {
        this.setState({
            title: event.target.value
        });
    }
    handleDateChange(event) {
        this.setState({
            date: event.target.value
        });
    }
    handlePlanDescriptionChange(event) {
        this.setState({
            description: event.target.value
        });
    }
    handlePlanRelevanceChange(event) {
        this.setState({
            relevance: event.target.value
        });
    }

    getData() {
        return localStorage.getItem('userId');
    }

    handleForm(event) {
        this.setState({
            buttonEnabled: false,
            requestResult: "creation.."
        });
     
        const headers = {
            'userID': this.getData() 
          }
       
        axios.post('http://34.89.147.190:8080/api/v001/partyhour/unitplans',
            {
               
                title: this.state.title,
                date: this.state.date,
                description: this.state.description,
                relevance: this.state.relevance
                
            },
           { headers: headers}
            )
            
            .then(r => {
                console.log(r)
                this.setState({ requestResult: "success" })
                console.log("ddd") 
                
            })
           .catch(e => {
                console.log(e)
                //this.setState({ requestResult: "fill in all input fields" })
                console.log("ggg")
            });
        this.setState({ buttonEnabled: true })
        event.preventDefault();
    }


    render() {
        return (
            <div className="reg">
                
                <Form vertical="true">
                <div className="row justify-content-center">
                <h1> New unit plan </h1>
                </div>
                    <input
                        className="form-control"
                        type="text"
                        placeholder="title"
                        value={this.state.title}
                        onChange={this.handlePlanNameChange}
                        required={true}
                    />
                    <label>
                        Please, enter date in format yyyy-mm-dd
                    <input
                        className="form-control"
                        type="text"
                        placeholder="date"
                        value={this.state.date}
                        onChange={this.handleDateChange}
                        required={true}
                    />
                    </label>
                    <input
                        className="form-control"
                        type="text"
                        placeholder="description"
                        value={this.state.description}
                        onChange={this.handlePlanDescriptionChange}
                        required={true}
                    />
                    <label>
                        Please, choose your plan relevanse.
                        1 -   high importance
                        2 -   medium importance
                        3 -   low importance
            
                <input
                            className="form-control"
                            type="text"
                            placeholder="relevance"
                            value={this.state.relevance}
                            onChange={this.handlePlanRelevanceChange}
                            required={true}
                        /> 
                        
                   
                    </label>
                    
                <div className="row justify-content-center">
                    <Button
                        className="btn btn-primary"
                        type="submit"
                        disabled={!this.state.buttonEnabled}
                        onClick={this.handleForm}
                    >
                        Create
                        </Button>
                </div>
                </Form>
                <div>
                    {this.state.requestResult}
                </div>





            </div>
        )
    }
}
