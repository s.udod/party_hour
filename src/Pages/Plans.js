import React, { Component } from 'react'
import { BrowserRouter,  Route,  NavLink } from "react-router-dom";
import UnitPlanCreation from './UnitPlanCreation';
import RegPlanCreation from './RegPlanCreation';
import axios from 'axios';
import RegPlanComp from './../Components/RegPlanComp';
import UnitPlanComp from './../Components/UnitPlanComp';


export default class Plans extends Component {

    state = {
        unitPlans: [],
        regPlans: []
    }
    
    getData() {
        return localStorage.getItem('userId');
    }
    
    regularPlanComponents() {
        return (
            this.state.regPlans.map(rp => {
                let days = rp.days.map(d => {
                    switch (d.number) {
                        case 1: return "ПН";
                        case 2: return "ВТ";
                        case 3: return "СР";
                        case 4: return "ЧТ";
                        case 5: return "ПТ";
                        case 6: return "СБ";
                        case 7: return "ВС";
                        default: return "?";
                    }
                });
                return <RegPlanComp title={rp.title} description={rp.description} relevance={rp.relevance} days={days} />
            }
            )
        )
    }

    componentDidMount() {
        axios.get('http://34.89.147.190:8080/api/v001/partyhour/unitplans', { headers: { 'userID': this.getData() } })
            .then(r => {
                this.setState({ unitPlans: r.data });
            })
            .catch(e => {
                console.log(e);
            });
        axios.get('http://34.89.147.190:8080/api/v001/partyhour/regplans', { headers: { 'userID': this.getData() } })
            .then(r => {
                console.log(r);
                this.setState({ regPlans: r.data });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        return (
            <BrowserRouter>
                <div>
                    <h1>My plans</h1>
                    <div>
                        <NavLink to="/regcreation">[Add regular plan]</NavLink>
                        <NavLink to="/unitcreation">[Add unit plan]</NavLink>
                    </div>
                    <div>
                        Unit plans list
                        {this.state.unitPlans.map(up => { return <UnitPlanComp title={up.title} date={up.date} description={up.description} relevance={up.relevance} /> })}
                    </div>
                    <div>
                        Regular plans list
                        {this.regularPlanComponents()}
                    </div>
                    <div>
                        <Route exact path='/regcreation' component={RegPlanCreation} />
                        <Route exact path='/unitcreation' component={UnitPlanCreation} />
                    </div>
                </div>
            </BrowserRouter>
        )
    }
}
