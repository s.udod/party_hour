import React, { Component } from 'react';
import axios from 'axios';
import { Form, Button } from "react-bootstrap";
import { Redirect, Link} from 'react-router-dom';


export default class SignIn extends Component {
    constructor(props) {
        super(props);
        let loggedIn = false
        this.state = {
            userid: "",
            userName: "",
            buttonEnabled: true,
            requestResult: "",
            loggedIn
        };
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLoginChange = this.handleLoginChange.bind(this);
    }

    
    setData() {
        localStorage.setItem('userId', this.state.userid);
    }
    getData() {
        console.log(localStorage.getItem('userId'));
        return localStorage.getItem('userId');
    }
    
    handleLoginChange(event) {
        const nextLoginToAdd = event.target.value;
        this.setState({
            userName: nextLoginToAdd
        });
    }
    
    handleLogin(event) {
        
            this.setState({
                buttonEnabled: false,
                requestResult: "logging in.."
            });
    
            axios.post(`http://34.89.147.190:8080/api/v001/partyhour/signin`, { userName: this.state.userName })
                .then(r => {
                    console.log(r)
                    this.setState({
                        requestResult: "success",
                        userid: r.data.userID,
                        loggedIn: true
                    })
                    this.setData()
                }).catch(e => {
                    console.log("login error", e)
                    this.setState({ requestResult: "bad data" })
                });
            this.setState({ buttonEnabled: true })
    }
    render() {
        if (this.state.loggedIn) {
            return <Redirect to="/main" />
        }
        return (
            <div className="myForm">
                <div className="row justify-content-center">
                    <h1>Party Hour</h1>
                </div>
                <Form vertical="true">
                    <input
                        className="form-control"
                        type="text"
                        placeholder="userName"
                        value={this.state.userName}
                        onChange={this.handleLoginChange}
                        required={true}
                    />
                </Form>
                <div className="row justify-content-center">
                    <Button
                        className="btn btn-primary"
                        type="submit"
                        disabled={!this.state.buttonEnabled}
                        onClick={this.handleLogin}
                    >
                        Sign In
                        </Button>
                </div>
                <Link to="/sign_up" >
                    Sign Out</Link>
                
                <div>
                    {this.state.requestResult}
                </div>
            </div>
        );

    }

}

