import React, { Component } from 'react';
import { Form, Button } from "react-bootstrap";
import axios from 'axios';
import {  Link} from 'react-router-dom';

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            firstName: "",
            secondName: "",
            pictureURL: "",
            buttonEnabled: true,
            requestResult: ""
        };
        this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleSecondNameChange = this.handleSecondNameChange.bind(this);
        this.handlePictureURLChange = this.handlePictureURLChange.bind(this);
        this.handleForm = this.handleForm.bind(this);
    }

    handleUserNameChange(event) {
        this.setState({
            userName: event.target.value
        });
    }
    handleFirstNameChange(event) {
        this.setState({
            firstName: event.target.value
        });
    }
    handleSecondNameChange(event) {
        this.setState({
            secondName: event.target.value
        });
    }
    handlePictureURLChange(event) {
        this.setState({
            pictureURL: event.target.value
        });
    }
    handleForm(event) {
       // event.preventDefault();
        this.setState({
            buttonEnabled: false,
            requestResult: "loading.."
        });

        axios.post(`http://34.89.147.190:8080/api/v001/partyhour/signup`,
            {
                userName: this.state.userName,
                firstName: this.state.firstName,
                secondName: this.state.secondName,
                pictureURL: this.state.pictureURL
            })
            .then(r => {
                console.log(r)
                this.setState({ requestResult: "success" })
            })
            .catch(e => {
                console.log(e)
                this.setState({ requestResult: "bad data"})
            })
            .finally(_ => {
                this.setState({buttonEnabled: true})
            });
        //this.setState({ buttonEnabled: true })
    
       
    }

    render() {
        return (
            <div className="myForm">
                <Form vertical = "true" >
                <div className="row justify-content-center">
                <h1>Party Hour</h1>
                </div>
                    <input
                        className="form-control"
                        type="text"
                        placeholder="userName"
                        value={this.state.userName}
                        onChange={this.handleUserNameChange}
                        required={true}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="firstName"
                        value={this.state.firstName}
                        onChange={this.handleFirstNameChange}
                        required={true}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="secondName"
                        value={this.state.secondName}
                        onChange={this.handleSecondNameChange}
                        required={true}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="pictureURL"
                        value={this.state.pictureURL}
                        onChange={this.handlePictureURLChange}
                    />
                    <div className="row justify-content-center">
                        <Button
                            className="btn btn-primary"
                            type="submit"
                            disabled={!this.state.buttonEnabled}
                            onClick={this.handleForm}
                        >
                            Sign Up
                    </Button>
                    </div>
                </Form>
                <Link to="/" > Sign In</Link>
                 <div>
                    {this.state.requestResult}
                </div>
            </div>
        )
    }
}
