import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Components/Header';
import './App.css'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SignIn from './Pages/SignIn';
import SignUp from './Pages/SignUp';

class App extends Component {

  render() {
    
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={SignIn} />
          <Route path="/main" component={Header} />
          <Route path="/sign_up" component={SignUp} />
        </Switch>
      </Router>

    )
  }
}
export default App;