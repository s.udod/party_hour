import React from 'react';

const ButtonComp = (props) => {
    return(
        <button>{props.text}</button>
    );
}

export default ButtonComp;