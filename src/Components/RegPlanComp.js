import React from 'react';

let RegPlanComp = (props) => {
    return (
        <div>
            <ul>
                <li>
                    <div>----{props.title}----</div>
                    <div>Description: {props.description}</div>
                    <div>Relevance: {props.relevance}</div>
                    <div>Days: {props.days.map(d=>d + " ")}</div>
                </li>
            </ul>
        </div>
    )
}

export default RegPlanComp;