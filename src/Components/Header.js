import React, { Component } from "react";
import { Navbar, Nav,  Container } from "react-bootstrap";
import { Route} from "react-router-dom";

import Profile from '../Pages/Profile';
import Events from '../Pages/Events';
import Plans from '../Pages/Plans';

export default class Header extends Component {
    render() {
        return (
            <>
                <Navbar collapseOnSelect expand="md" bg="success" variant="dark">
                    <Container>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link href="/main/profile">Profile</Nav.Link>
                                <Nav.Link href="/main/myplans">My plans</Nav.Link>
                                <Nav.Link href="/main/">Events</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                <Route exact path="/main/profile" component={Profile}/>
                <Route exact path="/main/myplans" component={Plans}/>
                <Route exact path="/main/" component={Events}/>
            </>
        );
    }
} 
