import React from 'react';

let UnitPlanComp = (props) => {
    return(
        <div>
            <ul>
                <li>
                    <div>----{props.title}----</div>
                    <div>Date: {props.date}</div>
                    <div>Relevance: {props.relevance}</div> 
                    <div>Description: {props.description}</div>
                </li>
            </ul>

        </div>
    )
}

export default UnitPlanComp;